import React from 'react';
import room1 from '../images/room1.jpg';
import room2 from '../images/room-2.jpg';
import room3 from '../images/room-3.jpg';
import room4 from '../images/room-4.jpg';
import room5 from '../images/room-5.jpg';
import room6 from '../images/room-6.jpg';
import room7 from '../images/room-7.jpg';
import room8 from '../images/room-8.jpg';
import room9 from '../images/room-9.jpg';
import room10 from '../images/room-10.jpg';
import Game from "./game.js";
import { BrowserRouter as Router, Switch,Route,Link } from "react-router-dom";
export default class Room extends React.Component {
    render() {
        return (
            <Router>
                <Switch>
                <Route exact path="/game" component={Game} />
                <section id="rooms" className="room">
                    <div className="container room-me">
                        <div className="row">
                            <div className="col d-flex room-cta-game">                                           
                                <img src={room1} className="img-fluid mx-auto" />    
                                <Link to={{pathname:"/game" }} className="room-cta-free" ></Link>                          
                            </div>  
                            <div className="col d-flex room-cta-game">                                           
                                <img src={room2} className="img-fluid mx-auto" />  
                                <Link to={{pathname:"/game"}} className="room-cta-free room-cta-btn_05"></Link>                            
                            </div>
                            <div className="col d-flex room-cta-game">                                           
                                <img src={room3} className="img-fluid mx-auto" /> 
                                <Link to={{pathname:"/game"}} className="room-cta-free room-cta-btn_01"></Link>                             
                            </div>
                            <div className="col d-flex room-cta-game">                                           
                                <img src={room4} className="img-fluid mx-auto" />      
                                <Link to={{pathname:"/game"}} className="room-cta-free room-cta-btn_02"></Link>                      
                            </div>
                            <div className="col d-flex room-cta-game">                                           
                                <img src={room5} className="img-fluid mx-auto" />  
                                <Link to={{pathname:"/game"}} className="room-cta-free room-cta-btn_05"></Link>                             
                            </div>                             
                        </div>
                        <div className="row mt-5 pb-5">
                            <div className="col d-flex room-cta-game">                                           
                                <img src={room6} className="img-fluid mx-auto" />                              
                            </div>
                            <div className="col d-flex room-cta-game">                                           
                                <img src={room7} className="img-fluid mx-auto" />                              
                            </div>
                            <div className="col d-flex room-cta-game">                                           
                                <img src={room8} className="img-fluid mx-auto" />                              
                            </div>
                            <div className="col d-flex room-cta-game">                                           
                                <img src={room9} className="img-fluid mx-auto" />                              
                            </div>
                            <div className="col d-flex room-cta-game">                                           
                                <img src={room10} className="img-fluid mx-auto" />                              
                            </div>
                        </div>
                    </div>
                </section>            
              </Switch>
            </Router>
                    
        )
    }
}