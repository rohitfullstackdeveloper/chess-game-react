import React from 'react';
import win_end from '../images/win_end.png';
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import Room from "./room.js";

export default class Start extends React.Component{
    render() {
        return(
            <Router>
                <Switch>
                    <Route exact path="/room" component={Room} />
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 d-flex cta">   
                                <img src={win_end} className="img-fluid mx-auto" />  
                                <Link to={{pathname:"/room" }} className="cta2" ></Link>
                            </div>
                        </div>
                    </div>
                </Switch>
                
            </Router>
        )
    }
}