import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import Start from './components/start.js'
import { BrowserRouter } from 'react-router-dom';


ReactDOM.render(
<BrowserRouter>
 <Start />
</BrowserRouter>,
  document.getElementById('root')
);

